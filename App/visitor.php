<?php

//require '../Entity/Visitor.php';

/**
 * Get a visitor's info
 * @param  int $id The ID of the visitor
 * @return json     The info found
 */
function view($id) {
    $app = \Slim\Slim::getInstance();
    $em = $app->config("em");
    $visitor = $em->getRepository('Visitor')->find($id);

    echo json_encode(array(
        "visitor" => $visitor->getArray()
    ));
}


/**
 * Make a payment to an event
 * @param  int $id The ID of the visitor
 * @return json     The info found
 */
function transferToEvent($id) {
    $app = \Slim\Slim::getInstance();
    $em = $app->config("em");
    $visitor = $em->getRepository('Visitor')->find($id);

    $req = $app->request();
    $amount = (int) $req->post("amount");

    $new = (int) $visitor->getTokens() - (int)$amount;

    $status = "failed";

    if($new >= 0)
    {
        $visitor->setTokens( $new );
        $em->flush();
        $status = "success";
    }

    echo json_encode(array(
        "status" => $status,
        "visitor" => $visitor->getArray()
    ));
}

/**
 * Add new tokens to the visitor
 * @param  int $id The ID of the visitor
 * @return json     The info found
 */
function reload($id) {
    $app = \Slim\Slim::getInstance();
    $em = $app->config("em");
    $visitor = $em->getRepository('Visitor')->find($id);

    $req = $app->request();
    $amount = (int) $req->post("amount");

    $new = (int) $visitor->getTokens() + (int)$amount;

    $visitor->setTokens( $new );
    $em->flush();
    $status = "success";

    echo json_encode(array(
        "status" => $status,
        "visitor" => $visitor->getArray()
    ));
}

/**
 * Check if a visitor has access to an event area
 */
function checkAccess($id, $area)
{
    $app = \Slim\Slim::getInstance();
    $em = $app->config("em");
    $visitor = $em->getRepository('Visitor')->find($id);
    $area = $em->getRepository('Area')->find($area);

    //$visitor->addArea($area);
    // $em->flush();
    $access = "refused";

    //$bla = new Visitor();
    echo "<pre>";
    foreach($visitor->getAreas() as $area)
    {
        var_dump($area->getName());
    }

    if($visitor->getAreas()->contains($area))
    {
        $access = "granted";
    }

    echo json_encode(array(
        "access" => $access,
        "visitor" => $visitor->getArray()
    ));
}
