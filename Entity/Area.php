<?php

/**
 * @Entity
 * @Table(name="areas")
 */
class Area
{
    /**
    * @Id
    * @Column(type="integer")
    * @GeneratedValue
     **/
    protected $id;

    /**
     * @Column(type="string")
     **/
    protected $name;

    /**
     * @ManyToMany(targetEntity="Visitor", mappedBy="areas")
     **/
    protected $visitors;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->visitors = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Area
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add visitors
     *
     * @param \Visitor $visitors
     * @return Area
     */
    public function addVisitor(Visitor $visitors)
    {
        $this->visitors[] = $visitors;

        return $this;
    }

    /**
     * Remove visitors
     *
     * @param \Visitor $visitors
     */
    public function removeVisitor(Visitor $visitors)
    {
        $this->visitors->removeElement($visitors);
    }

    /**
     * Get visitors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVisitors()
    {
        return $this->visitors;
    }
}
