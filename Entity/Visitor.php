<?php

/**
 * @Entity
 * @Table(name="visitors")
 **/
class Visitor
{
    /**
    * @Id
    * @Column(type="integer")
    * @GeneratedValue
    **/
    protected $id;

    /**
    * @Column(type="string")
    **/
    protected $name;

    /**
    * @Column(type="integer")
    **/
    protected $tokens;

    /**
     * @ManyToMany(targetEntity="Area", inversedBy="visitors")
     * @JoinTable(name="visitors_areas")
     **/
    protected $areas;

    public function getArray()
    {
        $ar = array(
            "id" => $this->getId(),
            "name" => $this->getName(),
            "tokens" => $this->getTokens()
            );
        return $ar;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->areas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Visitor
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set tokens
     *
     * @param integer $tokens
     * @return Visitor
     */
    public function setTokens($tokens)
    {
        $this->tokens = $tokens;

        return $this;
    }

    /**
     * Get tokens
     *
     * @return integer
     */
    public function getTokens()
    {
        return $this->tokens;
    }

    /**
     * Add areas
     *
     * @param \Area $areas
     * @return Visitor
     */
    public function addArea(Area $areas)
    {
        $this->areas[] = $areas;

        return $this;
    }

    /**
     * Remove areas
     *
     * @param \Area $areas
     */
    public function removeArea(Area $areas)
    {
        $this->areas->removeElement($areas);
    }

    /**
     * Get areas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAreas()
    {
        return $this->areas;
    }
}
