<?php
require 'doctrine-bootstrap.php';

$app = new \Slim\Slim();
$app->config("em", $em);

require 'App/index.php';

$app->get('/hello/:name', 'home');

// Visitor actions
$app->get('/visitor/:id', 'view');
$app->post('/visitor/:id/event-transfer', 'transferToEvent');
$app->post('/visitor/:id/reload', 'reload');
$app->get('/visitor/:id/access/:area', 'checkAccess');

$app->run();
