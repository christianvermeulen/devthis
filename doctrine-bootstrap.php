<?php

// Doctrine
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

require 'vendor/autoload.php';

$isDevMode = false;
$dbParams = array(
    'driver'   => 'pdo_mysql',
    'user'     => 'devthis',
    'password' => 'blaat',
    'dbname'   => 'devthis',
);
$config = Setup::createAnnotationMetadataConfiguration(array("Entity"), $isDevMode);
$em = EntityManager::create($dbParams, $config);
